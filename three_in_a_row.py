import os
from termcolor import colored, cprint


class Turn:
    turn_dic = {"red": colored(" O ", "red") + "|", "blue": colored(" X ", "blue") + "|"}
    turn_dic_key = {" O |": colored("red", "red"), " X |": colored("blue", "blue")}

    def __init__(self, turn="red"):
        self.__state = self.turn_dic[turn]

    def next_turn(self):
        if self.__state == self.turn_dic["red"]:
            print("Next turn is: " + colored("blue", "blue"))
            self.__state = self.turn_dic["blue"]
        else:
            print("Next turn is: " + colored("red", "red"))
            self.__state = self.turn_dic["red"]

    def cur_turn(self):
        return self.__state

    def get_key(self):
        return self.turn_dic_key[self.__state]




class Cell:
    __empty_cell = " _ |"

    def __init__(self, state=__empty_cell):
        self.__state = state

    def is_empty(self):
        return self.__state == self.__empty_cell

    def set(self, state):
        if self.is_empty():
            self.__state = state
            return True
        else:
            return False

    def state(self):
        return self.__state


class Field:

    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        self.__cells = []
        for i in range(0, x):
            arr = []
            for j in range(0, y):
                cell = Cell()
                arr.append(cell)
            self.__cells.append(arr)

    def show(self):
        os.system('cls')
        for x in range(0, self.__x):
            row = ""
            for y in range(0, self.__y):
                row += self.__cells[x][y].state()
            print(row)

    def get_cell(self, x, y):
        return self.__cells[x, y]

    def set_cell(self, x, y, turn):
        return self.__cells[x][y].set(turn)


class Game:

    def __init__(self, first_turn):
        width = False
        height = False
        self.__t = Turn(first_turn)
        self.__width = width
        self.__height = height
        while not self.__width:
            x = self.input_data("Type field width <= 100: ")
            if x > 2 and x < 101:
                self.__width = x
            else:
                print("You are too insatiable! Enter the number <= 100 or >= 3")
        while not self.__height:
            y = self.input_data("Type field height <= 100: ")
            if y > 2 and y < 101:
                self.__height = y
            else:
                print("You are too insatiable! Enter the number <= 100 or >= 3")
        success = False
        while not success:
            self.__l = self.input_data("Type length of sequence to win (l <= min(w, h)):")
            success = self.__l <= min(x, y)
        self.__f = Field(x, y)
        self.run()

    def input_data(self, invitation):
        while True:
            print(invitation)
            inp = input()
            if inp.isdigit() == False:
                print("This isn't a number, my dear friend\n" + invitation)
            elif inp == "":
                print("You didn't enter anything\n" + invitation)
            else:
                break
        return int(inp)

    def isInField(self, x, y):
        if self.__width > x and self.__height > y and -(self.__width) - 1 < x and -(self.__height) - 1 < y:
            return True

        else:
            return False

    def someone_won(self, symbolOfCurPlayer, Cells, countMaxWinSymb):
        # Checking three in a row


        for i in range(self.__height):  # горизонталь
            nInRow = 0
            for j in range(self.__width):
                if (Cells[i][j].state() != symbolOfCurPlayer):
                    nInRow = 0
                else:
                    nInRow += 1
                    if nInRow >= countMaxWinSymb:
                        return True

        for i in range(self.__width):  # вертикаль
            nInRow = 0
            for j in range(self.__height):
                if (Cells[i][j].state() != symbolOfCurPlayer):
                    nInRow = 0
                else:
                    nInRow += 1
                    if nInRow >= countMaxWinSymb:
                        return True

        for j in range(self.__height):  # диагональ
            nInRow = 0
            i = 0
            while (self.isInField(i, i + j)):
                if (Cells[i + j][i].state() != symbolOfCurPlayer):
                    nInRow = 0
                else:
                    nInRow += 1
                    if nInRow >= countMaxWinSymb:
                        return True
                i += 1

        for j in range(self.__height):  # обратная диагональ
            nInRow = 0
            i = 0
            while (self.isInField(-i - 1, i + j)):
                if (Cells[i + j][-i - 1].state() != symbolOfCurPlayer):
                    nInRow = 0
                else:
                    nInRow += 1
                    if nInRow >= countMaxWinSymb:
                        return True
                i += 1
        return False

    def run(self):
        while not self.someone_won(self.__t.cur_turn(), self.__f._Field__cells, self.__l):
            self.__f.show()
            self.turn()
        print("The winner is ", self.__t.get_key())

    def turn(self):
        success = False
        while not success:
            check_row = False
            check_column = False
            while not check_row:
                x = self.input_data("Type cell row:") - 1
                if x > -1 and x < self.__width:
                    check_row = True
                else:
                    print("This number is not included in the line")
            while not check_column:
                y = self.input_data("Type cell column:") - 1
                if y > -1 and y < self.__height:
                    check_column = True
                else:
                    print("This number is not included in the row")
            success = self.__f.set_cell(x, y, self.__t.cur_turn())
            print("Cell is not empty.")
        self.__t.next_turn()


print("Hello! We start!")
print(colored("X", "blue") + " is " + colored("blue", "blue") + " player, " + colored("O", "red") + " is " + colored("red", "red") + " player")

check_turn_input = False
while not check_turn_input:
    print("Who start the game? (type " + colored("blue", "blue") + " or " + colored("red", "red") + "):")
    first_turn = input()
    if first_turn == "red" or first_turn == "blue":
        check_turn_input = True
    else:
        print("incorrect input")

g = Game(first_turn)